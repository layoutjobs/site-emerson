<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Site Emerson</title>


    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/main.css" type="text/css">

    <!-- FontAwesome -->
    <script src="https://kit.fontawesome.com/7bc0885a91.js" crossorigin="anonymous"></script>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>

<body>


    <!-- JS -->
    <script src="assets/js/main.js" type="text/javascript"></script>
</body>

</html>