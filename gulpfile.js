// Chamadas de Funções
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();

// Funções para Compilar o Arquivo Scss
function compilarSass() {
    return gulp
        .src('styles/main.scss')
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(autoprefixer({
            Browserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('assets/css'))
        .pipe(browserSync.stream());
}


//Função para executar o comando sass
gulp.task('sass', compilarSass);


//Função para Concat do JS
function gulpJS() {
    return gulp.src(['node_modules/popper.js/dist/umd/popper.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/bootstrap/dist/js/bootstrap.min.js'])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('assets/js'))
        .pipe(browserSync.stream());
}

gulp.task('mainjs', gulpJS);


//Função para "Espionar as Alterações no Projeto"
function watchproject() {
    gulp.watch('styles/theme/*.scss', compilarSass);
    gulp.watch('scripts/*.js', gulpJS);
}

//Tarefa para iniciar o Watch
gulp.task('watch', watchproject);



//Tarefa padrão para executar o Gulp 
gulp.task('default', gulp.parallel('watch', 'sass', 'mainjs'));